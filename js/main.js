$(document).ready(function() {

	// prevents widows on words 7 letters and below, adjustable
	$('h1,h2,h3,li,p').each(function() {
	    $(this).html($(this).html().replace(/\s([^\s<]{0,9})\s*$/,'&nbsp;$1'));
	});

	//prevents standalone mode links from opening iOS safari, unless target="_blank"
	$(document).on('touchend click', 'a', function(e) {

	    if ($(this).attr('target') !== '_blank') {
	        e.preventDefault();
	        window.location = $(this).attr('href');
	    }

	});

});// End doc ready function
